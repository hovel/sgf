#ifndef SGF_HTTP_H
#define SGF_HTTP_H

#ifdef __cplusplus
extern "C" {
#endif

#include "apps/sgf_lvgl.h"

int sgf_http_get_weather(sgf_info_t *info, char *adcode, char *key);
int sgf_http_get_code(sgf_info_t *info, char *type);
#ifdef __cplusplus
} /* extern "C" */
#endif

#endif