#ifndef __SGF_SENSOR_H
#define __SGF_SENSOR_H

#ifdef __cplusplus
extern "C" {
#endif


uint8_t sgf_power_get(void);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif