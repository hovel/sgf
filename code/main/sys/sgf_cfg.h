#ifndef SGF_CFG_H
#define SGF_CFG_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    char key[200];
    char code[10];
} sgf_cfg_http_t;

typedef struct {
    char ssid[100];
    char passwd[100];
} sgf_cfg_wifi_t;

typedef struct {
    char remind[500];
} sgf_cfg_gui_t;

typedef struct {
    sgf_cfg_wifi_t wifi;
    sgf_cfg_http_t http;
    sgf_cfg_gui_t gui;
    uint32_t magic;
} sgf_cfg_t;


int sgf_cfg_write(sgf_cfg_t *cfg);
int sgf_cfg_read(sgf_cfg_t *cfg);
int sgf_cfg_init(void);
int sgf_cfg_reset(void);

char *sgf_get_http_code(void);
char *sgf_get_http_key(void);
char *sgf_get_gui_remind(void);
char *sgf_get_wifi_passwd(void);
char *sgf_get_wifi_ssid(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
