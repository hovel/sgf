#ifndef _ED42A13_H_
#define _ED42A13_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int e4v2_e042a13_init(void);
int e4v2_e042a13_stop(void);
int e4v2_e042a13_draw(uint8_t *wbuf, uint8_t pix_size, uint32_t hor_res, uint32_t ver_res);
int e4v2_e042a13_sleep(void);
int e4v2_e042a13_clean(void);
#endif
