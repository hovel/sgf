#ifndef _EDP_PORT_H_
#define _EDP_PORT_H_

#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "e042a13.h"

#if 1
    #define BUSY_Pin 2
    #define RES_Pin 1
    #define DC_Pin 0
    #define CS_Pin 7
    #define SCK_Pin 6
    #define SDI_Pin 5
    #define E_3V3_Pin 8
    #define EPD_POWER_ON gpio_set_level(E_3V3_Pin, 1)
    #define EPD_POWER_OFF gpio_set_level(E_3V3_Pin, 0)
#else
    #define BUSY_Pin 18
    #define RES_Pin 19
    #define DC_Pin 3
    #define CS_Pin 2
    #define SCK_Pin 1
    #define SDI_Pin 8
    #define EPD_POWER_ON 
    #define EPD_POWER_OFF 
#endif


#define MONOMSB_MODE 1
#define MONOLSB_MODE 2
#define RED_MODE 3

#define MAX_LINE_BYTES 50 // 50 = 400/8
#define MAX_COLUMN_BYTES 300

#define ALLSCREEN_GRAGHBYTES (MAX_LINE_BYTES * MAX_COLUMN_BYTES)

#define EPD_W21_MOSI_0 gpio_set_level(SDI_Pin, 0)
#define EPD_W21_MOSI_1 gpio_set_level(SDI_Pin, 1)

#define EPD_W21_CLK_0 gpio_set_level(SCK_Pin, 0)
#define EPD_W21_CLK_1 gpio_set_level(SCK_Pin, 1)

#define EPD_W21_CS_0 gpio_set_level(CS_Pin, 0)
#define EPD_W21_CS_1 gpio_set_level(CS_Pin, 1)

#define EPD_W21_DC_0 gpio_set_level(DC_Pin, 0)
#define EPD_W21_DC_1 gpio_set_level(DC_Pin, 1)
#define EPD_W21_RST_0 gpio_set_level(RES_Pin, 0)
#define EPD_W21_RST_1 gpio_set_level(RES_Pin, 1)
#define isEPD_W21_BUSY gpio_get_level(BUSY_Pin)



void spi_init(void);
void spi_delay(unsigned char xrate);
void spi_write(unsigned char value);

void epd_delay_us(uint32_t us);
void epd_delay_ms(uint32_t ms);
void epd_delay_s(uint32_t s);

void epd_power_off(void);
void epd_power_on(void);
#endif
