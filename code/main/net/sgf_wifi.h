

#ifndef SGF_WIFI_H
#define SGF_WIFI_H

#ifdef __cplusplus
extern "C" {
#endif

#include "apps/sgf_lvgl.h"

void sgf_wifi_reset(void);
int sgf_wifi_init(void);
int sgf_wifi_set_station(char *ssid, char *password);
int sgf_wifi_set_ap(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif

