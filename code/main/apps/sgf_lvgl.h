
#ifndef SGF_LVGL_H
#define SGF_LVGL_H

#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
    char daytemp[20];
    char nighttemp[20];
    char dayweather[20];
    char nightweather[20];
    char weather[20];
    char city[20];
} sgf_weather_t;

typedef struct {
    int year;
    int wday;
    int mday;
    int mon;
    sgf_weather_t weather;
    char remind[200];
    int8_t wifi;
    int8_t power;
} sgf_info_t;



void sgf_lvgl_display(sgf_info_t *info);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
