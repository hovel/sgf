/**
 * @file disp_driver.c
 */

#include "disp_driver.h"
#include "spi_port.h"
#include "e042a13.h"
#include "wft0420.h"
#include "esp_log.h"

static const char *TAG = "disp";
#define DEFAULT_EPD EPD_TP_E042A13

epd_driver_t epd_drv[] = {
    {
        .init = e4v2_e042a13_init,
        .clean = e4v2_e042a13_clean,
        .draw = e4v2_e042a13_draw,
    },
    {
        .init = e4v2_wft0420_init,
        .clean = e4v2_wft0420_clean,
        .draw =  e4v2_wft0420_draw,
    }
};

void disp_driver_init(void)
{
    ESP_LOGI(TAG, "epd init");
    spi_init();
    epd_drv[DEFAULT_EPD].init();
    epd_drv[DEFAULT_EPD].clean();
    ESP_LOGI(TAG, "epd init end");
}



void disp_driver_flush(lv_disp_drv_t * drv, const lv_area_t * area, lv_color_t * color_map)
{
    epd_drv[DEFAULT_EPD].draw(drv->draw_buf->buf1, sizeof(lv_color_t), drv->hor_res, drv->ver_res);
    // e4v2_e042a13_write_screen_full(drv->draw_buf->buf1, sizeof(lv_color_t), drv->hor_res, drv->ver_res);
    
    lv_disp_flush_ready(drv);
    ESP_LOGI(TAG, "flush epd end");
}

void disp_driver_rounder(lv_disp_drv_t * disp_drv, lv_area_t * area)
{

}

void disp_driver_set_px(lv_disp_drv_t * disp_drv, uint8_t * buf, lv_coord_t buf_w, lv_coord_t x, lv_coord_t y,
    lv_color_t color, lv_opa_t opa) 
{
    return;
}
