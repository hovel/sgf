
#include "lvgl/lvgl.h"
#include "sgf_lvgl.h"
#include "sgf_sensor.h"

LV_IMG_DECLARE(person);
LV_FONT_DECLARE(serif_light);
LV_FONT_DECLARE(serif_light18);
LV_FONT_DECLARE(sans21);
LV_IMG_DECLARE(sun);
LV_IMG_DECLARE(rainy_3);
LV_IMG_DECLARE(rainy_1);
LV_IMG_DECLARE(cold);
LV_IMG_DECLARE(cloud);
LV_IMG_DECLARE(warm);
LV_IMG_DECLARE(storm);

static lv_obj_t * lv_head_obj = NULL;
static lv_obj_t * lv_top_obj = NULL;
static lv_obj_t * lv_bottom_right = NULL;
static lv_obj_t * lv_bottom_left = NULL;

void basic_obj_create(void)
{
    static lv_obj_t * lv_rectangle = NULL;
    static lv_obj_t * lv_line = NULL;
    static lv_point_t line_points[] = { {170, 185}, {170, 285}};

    if (lv_head_obj == NULL) {
        lv_head_obj = lv_obj_create(lv_scr_act());
    }
    lv_obj_set_pos(lv_head_obj, 0, 0);
    lv_obj_set_size(lv_head_obj, 420, 19);
    lv_obj_set_style_bg_opa(lv_head_obj, 0, LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(lv_head_obj, 0, LV_STATE_DEFAULT);

    if (lv_top_obj == NULL) {
        lv_top_obj = lv_obj_create(lv_scr_act());
    }
    lv_obj_set_pos(lv_top_obj, 20, 20);
    lv_obj_set_size(lv_top_obj, 360, 140);
    lv_obj_set_style_border_color(lv_top_obj, lv_color_black(), LV_STATE_DEFAULT);
    lv_obj_set_style_radius(lv_top_obj, 20, LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(lv_top_obj, 1, LV_STATE_DEFAULT);

    if (lv_bottom_right == NULL) {
        lv_bottom_right = lv_obj_create(lv_scr_act());
    }
    lv_obj_set_pos(lv_bottom_right, 180, 170);
    lv_obj_set_size(lv_bottom_right, 220, 130);
    lv_obj_set_style_bg_opa(lv_bottom_right, 0, LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(lv_bottom_right, 0, LV_STATE_DEFAULT);
    
    if (lv_bottom_left == NULL) {
        lv_bottom_left = lv_obj_create(lv_scr_act());
    }
    lv_obj_set_pos(lv_bottom_left, 0, 170);
    lv_obj_set_size(lv_bottom_left, 180, 130);
    lv_obj_set_style_bg_opa(lv_bottom_left, 0, LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(lv_bottom_left, 0, LV_STATE_DEFAULT);

    if (lv_line == NULL) {
        lv_line = lv_line_create(lv_scr_act());
    }
    lv_line_set_points(lv_line, line_points, 2);
    lv_obj_set_style_line_color(lv_line, lv_color_black(), LV_STATE_DEFAULT);
    lv_obj_set_style_line_width(lv_line, 1, LV_STATE_DEFAULT);


    if (lv_rectangle == NULL) {
        lv_rectangle = lv_obj_create(lv_scr_act());
    }
    lv_obj_set_pos(lv_rectangle, 190, 180);
    lv_obj_set_size(lv_rectangle, 14, 14);
    lv_obj_set_style_radius(lv_rectangle, 4, LV_STATE_DEFAULT);
    lv_obj_set_style_img_recolor(lv_rectangle, lv_color_black(),LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(lv_rectangle, lv_color_black(),LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(lv_rectangle, LV_OPA_100, LV_STATE_DEFAULT);
}

void sgf_lvgl_top_object(lv_obj_t * top_obj, sgf_info_t *info)
{
    
    static lv_obj_t * lv_img = NULL;
    static lv_obj_t * lv_label_fit = NULL;
    static lv_obj_t * lv_label = NULL;
    if (lv_label_fit == NULL) {
        lv_label_fit = lv_obj_create(top_obj);
    }
    lv_obj_set_pos(lv_label_fit, 120, 20);
    lv_obj_set_size(lv_label_fit, 230, 100);
    lv_obj_set_style_border_width(lv_label_fit, 0, LV_STATE_DEFAULT);

    if (lv_img == NULL) {
        lv_img = lv_img_create(top_obj);
    }
    
    lv_obj_set_pos(lv_img, 15, 15);
    lv_img_set_src(lv_img, &person);

    if (lv_label == NULL) {
        lv_label = lv_label_create(lv_label_fit);
    }
    lv_obj_set_align(lv_label, LV_ALIGN_CENTER); 
    lv_label_set_long_mode(lv_label, LV_LABEL_LONG_WRAP);
    lv_obj_set_style_width(lv_label, 200, LV_STATE_DEFAULT);
    lv_obj_set_style_text_color(lv_label, lv_color_black(), LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(lv_label, &serif_light18, LV_STATE_DEFAULT);
    lv_obj_set_style_text_line_space(lv_label, 10, LV_STATE_DEFAULT);
    lv_obj_set_style_text_letter_space(lv_label, 3, LV_STATE_DEFAULT);
    if (strlen(info->remind) <= 0) {
        lv_label_set_text(lv_label, "红豆生南国春来发几枝\n愿君多采撷此物最相思");
    } else {
        lv_label_set_text(lv_label, info->remind);
    }
    
    lv_obj_set_style_text_align(lv_label, LV_TEXT_ALIGN_CENTER, LV_STATE_DEFAULT);
    
}


void sgf_lvgl_b_left_object(lv_obj_t * b_left_obj, sgf_info_t *info)
{
    static lv_obj_t * lv_label = NULL;
    static lv_obj_t * lv_weather = NULL;
    static lv_obj_t * lv_weather_pic = NULL;

    if (lv_label == NULL) {
        lv_label = lv_label_create(b_left_obj);
    }
    lv_obj_set_pos(lv_label, 10, 90);
    lv_obj_set_style_text_color(lv_label, lv_color_black(), LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(lv_label, &serif_light18, LV_STATE_DEFAULT);
    lv_obj_set_style_text_letter_space(lv_label, 1, LV_STATE_DEFAULT);
    lv_label_set_text_fmt(lv_label, "%s:  %s度",info->weather.city, info->weather.daytemp);

    if (lv_weather == NULL) {
        lv_weather = lv_label_create(b_left_obj);
    }
    lv_obj_set_pos(lv_weather, 75, 30);
    lv_obj_set_style_text_font(lv_weather, &serif_light18, LV_STATE_DEFAULT);
    lv_obj_set_style_text_color(lv_weather, lv_color_black(), LV_STATE_DEFAULT);
    lv_obj_set_style_text_letter_space(lv_weather, 1, LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(lv_weather, &sans21, LV_STATE_DEFAULT);
    lv_label_set_text_fmt(lv_weather, "%s", info->weather.dayweather);

    if (lv_weather_pic == NULL) {
        lv_weather_pic = lv_img_create(b_left_obj);
    }
    lv_obj_set_pos(lv_weather_pic, 10, 10);

    if (strstr(info->weather.dayweather, "小雨") != 0) {
        lv_img_set_src(lv_weather_pic, &rainy_1);
    } else if(strstr(info->weather.dayweather, "雨") != 0) {
        lv_img_set_src(lv_weather_pic, &rainy_3);
    } else if(strstr(info->weather.dayweather, "晴") != 0) {
        lv_img_set_src(lv_weather_pic, &sun);
    } else if(strstr(info->weather.dayweather, "雪") != 0) {
        lv_img_set_src(lv_weather_pic, &cold);
    } else if(strstr(info->weather.dayweather, "阴") != 0) {
        lv_img_set_src(lv_weather_pic, &cloud);
    } else if(strstr(info->weather.dayweather, "雷电") != 0) {
        lv_img_set_src(lv_weather_pic, &storm);
    } else if(strstr(info->weather.dayweather, "多云") != 0) {
        lv_img_set_src(lv_weather_pic, &warm);
    }

}

void sgf_lvgl_b_right_object(lv_obj_t * b_right_obj, sgf_info_t *info)
{
    static lv_obj_t * lv_week = NULL;
    static lv_obj_t * lv_time = NULL;
    
    if (lv_week == NULL) {
        lv_week = lv_label_create(b_right_obj);
    }
    
    lv_obj_align(lv_week, LV_ALIGN_CENTER, 10, -20);
    lv_obj_set_style_text_font(lv_week, &serif_light, LV_STATE_DEFAULT);
    switch (info->wday)
    {
    case 6:
        /* code */
        lv_label_set_text(lv_week, "星 期 日");
        break;
    case 0:
        /* code */
        lv_label_set_text(lv_week, "星 期 一");
        break;
    case 1:
        /* code */
        lv_label_set_text(lv_week, "星 期 二");
        break;
    case 2:
        /* code */
        lv_label_set_text(lv_week, "星 期 三");
        break;
    case 3:
        /* code */
        lv_label_set_text(lv_week, "星 期 四");
        break;
    case 4:
        /* code */
        lv_label_set_text(lv_week, "星 期 五");
        break;
    case 5:
        /* code */
        lv_label_set_text(lv_week, "星 期 六");
        break;
    default:
        lv_label_set_text(lv_week, " ");
        break;
    }
    

    if (lv_time == NULL) {
        lv_time = lv_label_create(b_right_obj);
    }
    
    lv_obj_align(lv_time, LV_ALIGN_CENTER, 10, 20);
    lv_obj_set_style_text_font(lv_time, &serif_light, LV_STATE_DEFAULT);
    lv_label_set_text_fmt(lv_time, "%4d年%2d月%2d日", info->year, info->mon, info->mday);
}

void sgf_lvgl_head_object(lv_obj_t * head_obj, sgf_info_t *info)
{
    static lv_obj_t * lv_power = NULL;
    static lv_obj_t * lv_power_p = NULL;
    static lv_obj_t * lv_power_val = NULL;
    // static lv_obj_t * lv_power_text = NULL;
    
    if (lv_power == NULL) {
        lv_power = lv_obj_create(head_obj);
    }
    if (lv_power_val == NULL) {
        lv_power_val = lv_obj_create(head_obj);
    }
    if (lv_power_p == NULL) {
        lv_power_p = lv_obj_create(head_obj);
    }


    lv_obj_align(lv_power, LV_ALIGN_LEFT_MID, 1, -2);
    lv_obj_set_size(lv_power, 25, 10);
    lv_obj_set_style_border_color(lv_power, lv_color_black(), LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(lv_power, 1, LV_STATE_DEFAULT);

    lv_obj_align(lv_power_val, LV_ALIGN_LEFT_MID, 1, -2);
    lv_obj_set_size(lv_power_val, info->power / 4, 6);
    lv_obj_set_style_img_recolor(lv_power_val, lv_color_black(),LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(lv_power_val, lv_color_black(),LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(lv_power_val, LV_OPA_100, LV_STATE_DEFAULT);

    lv_obj_align(lv_power_p, LV_ALIGN_LEFT_MID, 26, -2);
    lv_obj_set_size(lv_power_p, 2, 4);
    lv_obj_set_style_img_recolor(lv_power_p, lv_color_black(),LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(lv_power_p, lv_color_black(),LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(lv_power_p, LV_OPA_100, LV_STATE_DEFAULT);
}

void sgf_lvgl_display(sgf_info_t *info)
{
    info->power = sgf_power_get();

    basic_obj_create();
    sgf_lvgl_head_object(lv_head_obj, info);
    sgf_lvgl_top_object(lv_top_obj, info);
    sgf_lvgl_b_left_object(lv_bottom_left, info);
    sgf_lvgl_b_right_object(lv_bottom_right, info);
}


