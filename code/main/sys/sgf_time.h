
#ifndef SGF_TIME_H
#define SGF_TIME_H

#ifdef __cplusplus
extern "C" {
#endif

#include "apps/sgf_lvgl.h"

int sgf_time_get(sgf_info_t *info);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif